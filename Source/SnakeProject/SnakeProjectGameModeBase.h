// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEPROJECT_API ASnakeProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
